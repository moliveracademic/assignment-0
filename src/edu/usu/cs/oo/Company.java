package edu.usu.cs.oo;

public class Company {

	private String name;
	private Location location;
	
	/*
	 * Create the constructor, getters, setters, and anything else
	 * that is necessary to make Company work.
	 * 
	 * Note: This includes the toString() method.
	 */
	
	// this is what happens when someone doesn't specify what company they work for...
	// only a guess, probably not correct (the company, that is...the code is just fine)
	public Company(){
		this.name = "Public Lavatories, Incorporated";
		this.location = new Location("26 Crapper Way #2", "(212)222-1122", "Sewage Flats", "New York");
	}
	
	// this is what happens when someone specifies what company they work for...
	// not as much of a guess
	public Company(String compName, String address, String areaCode, String city, String state){
		this.name = compName;
		this.location = new Location(address, areaCode, city, state);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "\n" + this.name + this.location.toString();		
	}
	
	
}
