package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		
		Student anotherStudent = new Student("Mark Oliver", "A00577955", new Job("Senior Game Developer", 1000000, new Company("Blizzard Entertainment", "16215 Alton Parkway", "949", "Irvine", "California")));
		// could happen, but slim chance of the million a year...
		
		System.out.println(student);
		
		/*
		 * Print out the student information. 
		 */
		System.out.println(anotherStudent);
		
	}

}